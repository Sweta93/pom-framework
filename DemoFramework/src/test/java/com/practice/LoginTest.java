package com.practice;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import process.LoginProcess;

public class LoginTest {

	WebDriver driver;
	Properties prop = new Properties();
	FileInputStream Fis;
	LoginProcess a;

	@BeforeClass
	public void Intialization() throws IOException {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Driver\\chromedriver.exe");
		driver = new ChromeDriver();
		Fis = new FileInputStream(System.getProperty("user.dir") + "\\Properties\\config.properties");
		prop.load(Fis);

		driver.get(prop.getProperty("HomeURL"));

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.manage().window().maximize();

	}

	@Test(priority = 1)
	public void InValidlogin() throws InterruptedException, IOException {

		a = new LoginProcess(driver);
		a.InValidLoginuser();

	}

	@Test(priority = 2)
	public void Validlogin() throws InterruptedException, IOException {

		a = new LoginProcess(driver);
		a.ValidLoginuser();
		driver.navigate().back();

	}

	@Test(priority = 3)
	public void Password_Forgot() throws IOException {

		a = new LoginProcess(driver);
		a.forgotyourpwd();

	}

	@AfterClass
	public void CloseBrowser() throws InterruptedException {
		Thread.sleep(4000);
		driver.close();

	}

}
