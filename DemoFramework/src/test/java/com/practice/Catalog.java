package com.practice;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import process.CatalogProcess;
import process.CreateanaccountProcess;
import process.LoginProcess;


public class Catalog {

	WebDriver driver;
	Properties prop = new Properties();
	FileInputStream Fis;
	CatalogProcess p;
	


	@BeforeClass
	public void Intialization() throws IOException {
		
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
        driver= new ChromeDriver();
		Fis = new FileInputStream(System.getProperty("user.dir") + "\\Properties\\config.properties");
		prop.load(Fis);
		
		driver.get(prop.getProperty("HomeURL"));
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.manage().window().maximize();
	}
	
	@Test(priority=1)
	public void Validlogin() throws InterruptedException, IOException 
	{
		
		p= new CatalogProcess(driver);
		p.ValidLoginuser();
	    
		
	}
	
	@Test(priority=2)
	public void Catalog_for_Dresses() throws InterruptedException 
	{
		
		p= new CatalogProcess(driver);
	    p.Catalog_Option();
	    
	}
	
	
	@AfterClass
	public void CloseBrowser() throws InterruptedException {
		driver.close();	
}
	
}
