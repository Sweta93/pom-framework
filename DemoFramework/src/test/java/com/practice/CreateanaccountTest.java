package com.practice;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import process.CreateanaccountProcess;


public class CreateanaccountTest {

	WebDriver driver;
	Properties prop = new Properties();
	FileInputStream Fis;
	CreateanaccountProcess a;
	


	@BeforeClass
	public void Intialization() throws IOException {
		
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
        driver= new ChromeDriver();
		Fis = new FileInputStream(System.getProperty("user.dir") + "\\Properties\\config.properties");
		prop.load(Fis);
		
		driver.get(prop.getProperty("HomeURL"));
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	

	}
	
	@Test(priority=1)
	public void Account_Creation() throws InterruptedException, IOException {
		
		a= new CreateanaccountProcess(driver);
		
		a.Sign();
	

		
	}
	@Test(priority=2)
	public void Registration() throws InterruptedException {
		
		 //a= new CreateanaccountProcess(driver);
		a.PersonalInformation();
		
	}
	
	
	


	
@AfterClass
	public void CloseBrowser() throws InterruptedException {
		
		driver.close();
		


		
		
		
	}

}
