	package com.practice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import process.AddcartProcess;


public class AddcartTest {

	WebDriver driver;
	Properties prop = new Properties();
	FileInputStream Fis;
	AddcartProcess b;
	FileInputStream fis1;
	


	@BeforeClass
	public void Intialization() throws IOException {
		
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
        driver= new ChromeDriver();
		Fis = new FileInputStream(System.getProperty("user.dir") + "\\Properties\\config.properties");
		prop.load(Fis);
		
		//excel reader


		driver.get(prop.getProperty("HomeURL"));
		
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		
		driver.manage().window().maximize();
		
	

	}
	

	
	@Test(priority=1)
	public void Validlogin() throws IOException
	{
		
		b= new AddcartProcess(driver);
	    b.ValidLoginuser();
	   	
	}
	
	
	@Test(priority=2)
	public void Womens_AddtoCart() throws InterruptedException
	{
		
		//b= new AddcartProcess(driver);
	    b.Womens_collection();
	    Thread.sleep(6000);
	    		
	}
	
	
	
	@AfterClass
	public void CloseBrowser() 
	{
		
		driver.close();
			
	}

}
