package pageobjectmodel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utility.Method;

public class LoginObject extends Method {

	public LoginObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public static String Signin_page = "login";
	public static String Email_id="//input[@id='email']";
	public static String Passcode="passwd";
	public static String Submit ="//button[@id='SubmitLogin']";
	public static String Forgot_Passcode ="//a[text()='Forgot your password?']";
	public static String Forgot_email ="//input[@id='email']";
	public static String Forgot_button="//span[text()='Retrieve Password']";
	public static String Login_page="//a[@title='Back to Login']";
	
private	WebElement element = null;
	
	public WebElement Signin()
	{
		
		element = getElementByClassName(Signin_page);
		return element;
	}
	

	public WebElement Email()
	{
		
		element = getElementByXpath(Email_id);
		return element;
	}
	
	public WebElement Password()
	{
		
		element = getElementByName(Passcode);
		return element;
	}

	
	public WebElement Submit_button()
	{
		
		element = getElementByXpath(Submit);
		return element;
	}
	
	public WebElement Forgot_Password()
	{
		
		element = getElementByXpath(Forgot_Passcode);
		return element;
	}
	
	public WebElement Forgot_EmailAddress()
	{
		
		element = getElementByXpath(Forgot_email);
		return element;
	}
	
	public WebElement Retrieve_Button()
	{
		
		element = getElementByXpath(Forgot_button);
		return element;
	}
	
	public WebElement Back_to_Login()
	{
		
		element = getElementByXpath(Login_page);
		return element;
	}
}
