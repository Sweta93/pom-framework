package pageobjectmodel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utility.Method;

public class CreateanaccountObject extends Method {

	public CreateanaccountObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public static String Signin_page = "login";
	public static String Email_address ="//input[@name='email_create']";
	public static String Create_account ="SubmitCreate";
	public static String GenderName ="//input[@id='id_gender1']";
	public static String FirstName ="//input[@id='customer_firstname']";
	public static String LastName ="//input[@id='customer_lastname']";
	public static String Password ="//input[@name='passwd']";
	public static String Add_01="//input[@id='firstname']";
	public static String Add_02="//input[@id='lastname']";
	public static String Add_03="//input[@name='address1']";
	public static String Add_04= "//input[@id='city']";
	public static String State= "//select[@id='id_state']";
	public static String Add_06="phone_mobile";
	public static String Add_07="//span[text()='Register']";
	public static String Zip ="postcode";
	
private	WebElement element = null;
	
	public WebElement Signin() 
	{
		
		element = getElementByClassName(Signin_page);
		return element;
	}
	
     public WebElement Email() 
     {
		
		element = getElementByXpath(Email_address);
		return element;
	}
     
     public WebElement Button() 
     {
    	 
    	 element =getElementByID(Create_account);
    	 return element;
     }

	public WebElement Gender() 
	{
		
		element = getElementByXpath(GenderName);
		return element;
			
	}
	
	public WebElement Cust_FirstName() {
		
		element = getElementByXpath(FirstName);
		return element;
		
		}

	
public WebElement Cust_LastName() 
{
		
		element = getElementByXpath(LastName);
		return element;
			
	}

public WebElement Cust_Password() 
{
	
	element = getElementByXpath(Password);
	return element;
		
}

public WebElement Add_Firstname() 
{
	
	element = getElementByXpath(Add_01);
	return element;
		
}

public WebElement Add_Lastname() 
{
	
	element = getElementByXpath(Add_02);
	return element;
		
}

public WebElement Add_Res() 
{
	
	element = getElementByXpath(Add_03);
	return element;
		
}

public WebElement Add_City() 
{
	
	element = getElementByXpath(Add_04);
	return element;
		
}

public WebElement Postalcode() 
{
	 
	 element =getElementByID(Zip);
	 return element;
}

public WebElement Add_State() 
{
	element = getElementByXpath(State);
	Select t= new Select(element);
	t.selectByVisibleText("Alaska");
	return element;
		
}



public WebElement Add_Phone() 
{
	
	element = getElementByName(Add_06);
	return element;
		
}


public WebElement Reg_button() 
{
	
	element = getElementByXpath(Add_07);
	return element;
		
}



}
