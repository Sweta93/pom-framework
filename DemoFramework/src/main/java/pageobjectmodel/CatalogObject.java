package pageobjectmodel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import utility.Method;

public class CatalogObject extends Method {

	public CatalogObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

public static String Signin_page = "login";
public static String Email_id="//input[@name='email']";
public static String Passcode="passwd";
public static String Submit ="//button[@id='SubmitLogin']";
public static String Womens_Wear ="//a[@title='Women']";
public static String Summmer="//a[@title='Summer Dresses']";
public static String Size="//a[text()='L']";
public static String Compositions="//a[text()='Viscose']";
//public static String Range ="//select[@id='selectProductSort']";

private	WebElement element = null;

public WebElement Signin()
{
	
	element = getElementByClassName(Signin_page);
	return element;
}


public WebElement Email()
{
	
	element = getElementByXpath(Email_id);
	return element;
}

public WebElement Password()
{
	
	element = getElementByName(Passcode);
	return element;
}


public WebElement Submit_button()
{
	
	element = getElementByXpath(Submit);
	return element;
}

public WebElement Women()
{
	element = getElementByXpath(Womens_Wear);
	Actions Act= new Actions(driver);
	Act.moveToElement(element).build().perform();
	return element;
}

public WebElement Summer_Dresses()
{
	
	element = getElementByXpath(Summmer);
	return element;
}

public WebElement Catalog_Size_01()
{
	
	element = getElementByXpath(Size);
	return element;
}

public WebElement Catalog__Compositions_01()
{
	
	element = getElementByXpath(Compositions);
	return element;
}


//public WebElement Sort_By_Range()
//{
	
	//element = getElementByXpath(Range);
	//Select t = new Select(element);
	//t.deselectByVisibleText("Price: Lowest first");
	//return element;
//}





}
