package pageobjectmodel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import utility.Method;

public class AddcartObject extends Method {

	public AddcartObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public static String Signin_page = "login";
	public static String Email_id="//input[@name='email']";
	public static String Passcode="passwd";
	public static String Submit ="//button[@id='SubmitLogin']";
	public static String Womens_wear ="//a[@title='Women']";
	public static String Womens_Casual ="(//a[text()='Casual Dresses'])[1]" ;
	public static String Womens_image ="//img[@title='Printed Dress']";
	public static String Womens_Cart01 = "//a[@title='Add to cart']";
	public static String Checkout ="//a[@title='Proceed to checkout']";
	public static String Checkout_01 ="//span[text()='Proceed to checkout']";
	public static String Checkout_Add ="//textarea[@name='message']";
	public static String Checkout_Add01 = "//span[text()='Proceed to checkout']";
	public static String Terms_Checkbox = "//p[@class='checkbox']/div";
	
	
private	WebElement element = null;
	
	public WebElement Signin()
	{
		
		element = getElementByClassName(Signin_page);
		return element;
	}
	

	public WebElement Email()
	{
		
		element = getElementByXpath(Email_id);
		return element;
	}
	
	public WebElement Password()
	{
		
		element = getElementByName(Passcode);
		return element;
	}

	
	public WebElement Submit_button()
	{
		
		element = getElementByXpath(Submit);
		return element;
	}
	
	public WebElement Womens_Tab()
	{
		element = getElementByXpath(Womens_wear);
		Actions act = new Actions(driver);
		act.moveToElement(element).perform();
		
		
		return element;
	}
	
	public WebElement Casuals()
	{
		
		element = getElementByXpath(Womens_Casual);
		return element;
	}
		
		
	 public WebElement Womensimg_hover()
		{
			
		 element = getElementByXpath( Womens_image );
		 	Actions act = new Actions(driver);
		 	act.moveToElement(element).perform();
			return element;
		}
	
	
	 public WebElement Womens_PrintedDress()
	{
		
	 	element = getElementByXpath( Womens_Cart01 );
	 	Actions act = new Actions(driver);
	 	act.moveToElement(element).build().perform();
	 	return element;
	}

	 
	 public WebElement Proceed_to_checkout()
		{
			
		 	element = getElementByXpath( Checkout );
		 	return element;
		}
	 
	 
	 public WebElement Proceed_to_checkout01()
		{
			
		 	element = getElementByXpath( Checkout_01  );
		 	return element;
		}

	 
	 public WebElement Checkout_Address()
		{
			
		 	element = getElementByXpath(Checkout_Add);
		 	return element;
		}

	 
	 public WebElement Checkout_Address1()
		{
			
		 	element = getElementByXpath(Checkout_Add01);
		 	return element;
		}
	 
	 public WebElement Terms_Condtions()
		{
			
		 	element = getElementByXpath(Terms_Checkbox );
		 	return element;
		}
	 
	 
	
	
}
	
	