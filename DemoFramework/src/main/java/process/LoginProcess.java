package process;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import pageobjectmodel.CreateanaccountObject;
import pageobjectmodel.LoginObject;
import utility.ExcelDriver;
import utility.Method;

public class LoginProcess extends Method {

	public LoginProcess(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	LoginObject P =new LoginObject(driver);
	
	public void InValidLoginuser() throws IOException 
	{
       P.Signin().click();
       P.Email().sendKeys(ExcelDriver.ReadExcel("Sheet1", 3, 1));
       P.Password().sendKeys(ExcelDriver.ReadExcel("Sheet1", 1, 3));
       P.Submit_button().click();
       P.Email().clear();
       P.Password().clear();
      
		
	}
	
	public void ValidLoginuser() throws IOException 
	{
       P.Email().sendKeys(ExcelDriver.ReadExcel("Sheet1", 1, 0));
       P.Password().sendKeys(ExcelDriver.ReadExcel("Sheet1", 2, 3));
       P.Submit_button().click();
	
	}
	
	

	public void forgotyourpwd() throws IOException 
	{
       P.Forgot_Password().click();
       P.Forgot_EmailAddress().sendKeys(ExcelDriver.ReadExcel("Sheet1", 1, 0));
       P.Retrieve_Button().click();
       P.Back_to_Login().click();
		
	}
	
	
	
	
	}

