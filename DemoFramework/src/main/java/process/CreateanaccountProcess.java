package process;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import pageobjectmodel.CreateanaccountObject;
import utility.ExcelDriver;
import utility.Method;

public class CreateanaccountProcess extends Method {

	public CreateanaccountProcess(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	CreateanaccountObject R =new CreateanaccountObject(driver);
	
	public void Sign() throws IOException 
	{
		
		R.Signin().click();
		R.Email().sendKeys(ExcelDriver.ReadExcel("Sheet1", 4, 0));
		R.Button().click();
		
	}
	
	public void PersonalInformation() throws InterruptedException
	{
		
		R.Gender().click();
		R.Cust_FirstName().sendKeys("Rahul");
		R.Cust_LastName().sendKeys("Singh");
		R.Cust_Password().sendKeys("Test1234");
		R.Add_Firstname().sendKeys("ABCD");
		R.Add_Lastname().sendKeys("XYZ");
		R.Add_Res().sendKeys("Mumbai 400101");
		R.Add_City().sendKeys("Mumbai");
		R.Add_City().sendKeys(Keys.TAB);
		
		R.Add_City().sendKeys(Keys.ENTER);
		
		
		Thread.sleep(3000);
		R.Add_State();
		
		R.Postalcode().sendKeys("00000");
		R.Add_Phone().sendKeys("9123458765");
		//R.Reg_button().click();
	}
	
	
	}

