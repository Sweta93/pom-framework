package process;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import pageobjectmodel.AddcartObject;
import utility.ExcelDriver;
import utility.Method;

public class AddcartProcess extends Method {

	public AddcartProcess(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	// Java Script Executor for scrolling down 
	JavascriptExecutor js = (JavascriptExecutor) driver;

	AddcartObject l =new AddcartObject(driver);
	
	public void ValidLoginuser() throws IOException 
	{
		l.Signin().click();
       l.Email().sendKeys(ExcelDriver.ReadExcel("Sheet1", 1, 0));
       l.Password().sendKeys(ExcelDriver.ReadExcel("Sheet1", 2, 3));
       l.Submit_button().click();
	
	}
	
	

	public void Womens_collection() throws InterruptedException {
		// TODO Auto-generated method stub
		
		l.Womens_Tab();
		l.Casuals().click();
	   js.executeScript("window.scrollBy(0,500)");
		Thread.sleep(3000);
		l.Womensimg_hover();
		Thread.sleep(3000);
		l.Womens_PrintedDress();
		l.Womens_PrintedDress().click();
		l.Proceed_to_checkout().click();
		l.Proceed_to_checkout01().click();
		l.Checkout_Address().sendKeys("Dress");
		l.Checkout_Address1().click();
		js.executeScript("window.scrollBy(0,1000)");
		l.Terms_Condtions().click();
		
	}

	

	}

