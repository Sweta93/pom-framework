package process;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import pageobjectmodel.CatalogObject;
import utility.ExcelDriver;
import utility.Method;

public class CatalogProcess extends Method {

	public CatalogProcess(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	JavascriptExecutor js = (JavascriptExecutor) driver;

	CatalogObject w = new CatalogObject(driver);

	public void ValidLoginuser() throws IOException {
		w.Signin().click();
		w.Email().sendKeys(ExcelDriver.ReadExcel("Sheet1", 1, 0));
		w.Password().sendKeys(ExcelDriver.ReadExcel("Sheet1", 2, 3));
		w.Submit_button().click();

	}

	public void Catalog_Option() throws InterruptedException {
		w.Women();
		w.Summer_Dresses().click();
		w.Catalog_Size_01().click();
		js.executeScript("window.scrollBy(0,1000)");
		w.Catalog__Compositions_01().click();
		
	}
	
	
}