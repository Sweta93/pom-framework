package utility;

import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class Method  {
	
	public static WebDriver driver;
	public static Wait<WebDriver> wait;
	
	protected static final long Fluent_Time =60;
	protected static final long Pollingtime =10;
	
	
	public Method(WebDriver driver) {
		
		Method.driver=driver;
		
		wait=new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(Fluent_Time))
		.pollingEvery(Duration.ofSeconds(Pollingtime)).ignoring(NoSuchElementException.class); 

}
	
	protected static WebElement getElementByID(String elementlocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(elementlocator)));
	}
	
	protected static WebElement getElementByName(String elementlocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(elementlocator)));
	}
	
	protected static WebElement getElementByXpath(String elementlocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementlocator)));
	}
	
	protected static WebElement getElementByClassName(String elementlocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(elementlocator)));
	}
	
	
	
	
	
	}

	

